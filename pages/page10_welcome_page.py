from page_objects import PageObject, PageElement


class WelcomePage(PageObject):
    def check_page(self):
        return "Welcome" in self.w.title

    login_link = PageElement(link_text="Login")

    def click_login(self, loginpage):
        self.login_link.click()
        return loginpage.check_page()
